# Circular Travel Activity

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 9/25/2020

Note: All given angles in this document, unless specified otherwise, are in reference to each other, and to the common angle of $0^\circ$ (horizontal, to the right).

# Velocity

![image-20200928233120321](circularTravelActivity.assets/image-20200928233120321.png)
$$
\overrightarrow {r_A}=241.7\ang54.70^\circ\\\overrightarrow {r_B}=244.8\ang329.87^\circ\\\overrightarrow {\Delta r}=328.0\ang282.68^\circ\\\overrightarrow {\Delta r'}=103.2\ang314.23^\circ\\
$$

1. What is the direction of the average velocity $\overrightarrow{v_{av}}$for the motion from A to B?
    Velocity's direction is the same as the average displacement's direction. (Time is scalar, so it does not affect the direction.) Therefore, the average velocity is $77.32^\circ$ down from the horizontal, or $282.68^\circ$ up from the horizontal.
2. If you moved $B'$ closer to A, what is the mathematical term for the direction of the displacement vector relative to the trajectory?
    The velocity vector will be normal to the displacement vector. That is, they will be $90^\circ$ apart.
3. What is the direction of the instantaneous velocity relative to the trajectory at any point on the circle?
    $\theta+90^\circ$
4. Does the direction of the instantaneous velocity at point A depend on the cars acceleration?
    No. 
5. Would moving the origin change the answers to 1 and 2 above? Why/Why not?
    Moving the origin would change answers, because there is a new reference, and angles will change.

# Acceleration

![image-20200929153913495](circularTravelActivity.assets/image-20200929153913495.png)
$$
\overrightarrow {s_A}=257.9\ang54.20^\circ\\
\overrightarrow {s_B}=262.2\ang330.15^\circ\\
\overrightarrow {s_B'}=262.3\ang29.97^\circ\\
\overrightarrow{v_A}=50\ang324.2^\circ\\
\overrightarrow{v_B}=50\ang240.15^\circ\\
\overrightarrow{v_B'}=50\ang299.97^\circ\\
\overrightarrow{\Delta v}=66.4\ang161.65^\circ\\
\overrightarrow{\Delta v'}=21.2\ang221.18^\circ\\
$$

6. Is the angle between the change in velocity $\Delta \overrightarrow{v}$ and the vector $\overrightarrow {v_A}$ less than, equal to, or greater than $90^\circ$?
    The angular difference between the two vectors is more than $90^\circ$.
7. Will the angle between $\overrightarrow{v_A}$ and $\Delta\overrightarrow{v}$ change? If so, will it get closer or farther from $90^\circ$?
    The angle changes, and it approaches $90^\circ$.
8. What is the limiting value for the angle between $\overrightarrow{v_A}$ and $\Delta\overrightarrow{v}$ as B approaches A?
    $90^\circ$
9. how are the directions of the average acceleration and the change in velocity related?
    The direction of the average acceleration and the change in velocity are $90^\circ$ apart.
10. For the car moving at constant speed, what is the angle between the instantaneous velocity and the acceleration, and how would you describe the direction of the acceleration?
    The angle is $90^\circ$, and the direction is towards the center of the circle.
11. For a car moving at a constant speed around a circular arc, does the direction of the instantaneous acceleration depend on whether the car is moving clockwise or counterclockwise around the arc?
    No.


$$
\overrightarrow{s_1}=127.7\ang41.09^\circ\\
\overrightarrow{s_2}=131.1\ang10.56^\circ\\
\overrightarrow{s_3}=50.2\ang139.45^\circ\\
\overrightarrow{s_3}=49.6\ang234.89^\circ\\
\overrightarrow{v_1}=50\ang311.09^\circ\\
\overrightarrow{v_2}=50\ang280.56^\circ\\
\overrightarrow{v_3}=50\ang229.45^\circ\\
\overrightarrow{v_4}=50\ang324.89^\circ
$$

12. Is the magnitude of $\Delta \overrightarrow v$ the same in both intervals? If not, in which is it larger?
    The magnitude of $\Delta \overrightarrow v$ is larger in the interval between 1 and 2. 
13. Write a clear statement regarding the magnitude of the acceleration as it relates to the radius of the curvature. Also, describe the direction of the acceleration. 
    The acceleration is inversely proportional to the radius of the curvature, and is always towards the center of the curvature.
14. What is triskaiidekaphobia?
    The fear of the number 13.

![image-20201003174001645](circularTravelActivity.assets/image-20201003174001645.png)

