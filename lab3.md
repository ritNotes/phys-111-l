# Week 9 Lab: Rotational Energy in Rolling Motion

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due: 10/23/2020

# Theory

A rigid body of mass $m$, radius $R$, and moment of inertia $I$, is released from rest at the top of an inclined plane. It rolls smoothly without slipping down the incline. Find the linear speed of the object's center-of-mass at the bottom of the incline, $v_1$.

1. Draw a free body diagram.
2. Which forces in your free body diagram are conservative?
    Kinetic and gravitational potential energy.
3. Is mechanical energy conserved as the object rolls down the incline?
    Yes
4. Describe the energy transformations that occur as the object rolls down the incline.
    The gravitational potential energy transforms into kinetic energy. 23.

For the following, choose the zero for the gravitational potential energy as the lowest point of motion. Identify the *initial* point as the instant the object is released from rest at the top of the incline, and the *final* point as the instant it reaches the bottom of the incline.

5. Write an expression for the total mechanical energy of the object in the *initial* position.
6. ... in the *final* position. 
7. Determine an expression for the translational speed of the center of mass at the bottom of the incline $v_1$. [^1]



## Pre-Video

Look up the *Moments of Inertia* table on the formula sheet.

8. What is the moment of inertia of a thin-walled ring of mass $m$ and radius $R$?
    $$
    I=mR^2
    $$
    
9. What is the moment of inertia of a solid disk of mass $m$ and radius $R$?
    $$
    I=\frac12mR^2
    $$
    



# Applications

***Before watching the video***, record your predictions and explanations for each case. Then watch the video, record your observations and any differences to the predictions. 

## Case 1

2 thin-walled rings of different radii are released from rest simultaneously at the top of the incline.

### Prediction:

Which will reach the bottom first?

The ring with the smaller radius will reach the bottom first; because $I$ is smaller when $R$ is smaller.

### Observation:

Which ring reached the bottom first? Explain any discrepancies between your observation and prediction.

The smaller ring reached the bottom first.

## Case 2

A solid disc and a thin-walled ring with the same radius are released simultaneously at the top of an incline.

### Prediction:

Which will reach the bottom first?

The solid disc will reach the bottom first, because $I$ is smaller with a solid disc than with a ring.

### Observation:

Which ring reached the bottom first? Explain any discrepancies between your observation and prediction.

The objects reached the bottom at the same time. This could be due to a difference in mass, or due to the slight difference in radius of the two objects.

## Case 3

2 thin-walled rings of different radii are released from rest simultaneously at the top of an incline. The rings roll down the incline and back up another incline.

### Prediction:

Which ring will roll further up the second incline?

They will roll the same amount up the second incline, because there are no non-conservative forces at play.

### Observation:

Which ring rolled farther up the second incline? Explain any discrepancies between your observation and prediction.

They both rolled up the same amount.

## Case 4

A solid disc and a thin-walled ring with the same radii are released from rest simultaneously at the top of an incline. The objects roll down the incline and back up another incline.

### Prediction:

Which ring will roll further up the second incline?

They will roll the same amount up the second incline, because there are no non-conservative forces at play.

### Observation:

Which ring rolled farther up the second incline? Explain any discrepancies between your observation and prediction.

They both rolled up the same amount.

# Rolling Energy Problem

1. Stating from rest at a distance $y_0$ above the ground, a basketball rolls without slipping down a ramp as shown. The ball leaves the ramp vertically when it is a distance $y_1$ above the ground with a center-of-mass speed $v_1$. Treat the ball as a thin-walled spherical shell. Ignore air resistance.

    1. What is the ball's speed $v_1$ the instant it leaves the ramp? [^1]
    $$
        \Delta K=-\Delta U\\
    \Delta U=mg\Delta y;\ \Delta K=\frac12m(v_f^2-v_i^2)+\frac12I\omega^2;\ \omega=\frac vr;\ I=\frac23MR^2\\
        -mg\Delta y=\frac12m(v_f^2-v_i^2)+\frac12I\omega^2\\
    -\cancel{m}^1g\Delta y=\frac12\cancel{m}^1(v_f^2-v_i^2)+\frac12*\frac23\cancel{M}^1\cancel{R^2}^1*(\frac{v_f^2-v_i^2}{\cancel{r^2}^1})\\
        -g\Delta y=\frac12(v_f^2\cancel{-v_i^2}^0)+\frac12*\frac23*(v_f^2\cancel{-v_i^2}^0)\\
        -g\Delta y=v_f^2(\frac12+\frac13)\\
        -\frac65g\Delta y=v_f^2\\
        \sqrt{-\frac65g(y_1-y_0)}=v_1
    $$
    
    
    2. What is the maximum height above the ground $H$ that the ball hits? [^1]
        $$
        v_f^2=v_i^2+2a\Delta y\\
        0=\frac65g(y_1-y_0)+2g(H-y_1)\\
        2g(H-y_1)=-\frac65g(y_1-y_0)\\
        H=y_1-\frac35(y_1-y_0)\\
        H=\frac{3y_0+2y_1}5
        $$
        
    3. Explain why $H\ne y_0$, using correct physics principles.
    
        $H=y_0$, if the track follows the ball for the complete duration of the movement. If it does not, then the rotational velocity is removed once hitting $y_1$, leading to a lower $H$.
    
    4. Determine the numerical values for $v_1$ and $H$ given:
        $$
        y_0=2.00m\\y_1=0.95m
        $$
        
    
    $$
    \sqrt{-\frac65g(y_1-y_0)}=v_1;\ H=\frac{3y_0-8y_1}5\\
    v_1=3.515764497m/s;\ H=1.58m\\
    \overline{\underline{|v_1=3.52m/s;\ H=1.58m|}}
    $$
    
    

