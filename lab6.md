# Standing Waves on a String

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss

#### Due Date: 11/20/2020

$$
\lambda=\frac vf\\
v=\sqrt{\frac T \mu}=2f_0L\\
f_0=fundamental\ frequency\\
n=harmonic\\
2L=wavelength\ of\ fundamental\ wave
$$

# Frequency determination for Two modes

Set linear mass density to $\mu\approx2.00\times10^{-3}kg/m$ and tension to $T=55.00N$, according to instructions.

(All numbers in this portion of the lab are found using the $\mu$ of $2.00\times10^{-3}kg/m$)

1. Calculate the speed $v$ of any wave in this string at this tension, and show the calculation.
    $$
    v=\sqrt{\frac{T}{\mu}}\\
    v=\sqrt{\frac{55.00N}{2.00\times10^{-3}kg/m}}\\
    v=165.8312395m/s\\
    \overline{\underline{|v=166.m/s|}}
    $$

2. What is the purpose of the mass hanging on the end of the string?
    To provide tension in the string so that a wave can be simulated. Waves on strings cannot exist without tension.

    1. Calculate the magnitude of the hanging mass:
        $$
        F=ma;\ a=g\\
        m=\frac Fg\\
        m=\frac{55.00N}{9.81m/s^2}\\
        m=5.606523955kg\\
        \overline{\underline{|m=5.61kg|}}
        $$

3. Use the slider under the frequency values to adjust the frequency such that you see the third mode for a standing wave in this string. ($n=3$) Record the frequency. Record the wavelength as well.
    ![image-20201117144238590](lab6.assets/image-20201117144238590.png)
    $$
    f_{n=3}=61.66Hz\\
    \lambda_{n=3}=\frac{L_m}{L_{px}}\times\lambda_{px}\\\lambda_{n=3}=(\frac{4.0m}{2000px}\times1355.8px)\\
    \lambda_{n=3}=2.7116m=2.7m
    $$

4. Repeat step 3 for $n=4$.
    ![image-20201117144246928](lab6.assets/image-20201117144246928.png)
    $$
    f_{n=4}=82.92Hz\\
    \lambda=(\frac{4.0m}{2000px}\times1000px)=2.0m
    $$

5. Calculate the speed of the wave for both $n=3, 4$. How do they compare?
    $$
    v=f\lambda\\
    v_3=61.66Hz*2.7m|v_4=82.92Hz*2.0m\\
    v_3=166.482m/s|v_4=165.84m/s\\
    \overline{\underline{|v_3=166m/s|v_4=166m/s|}}
    $$
    These values are functionally equivalent, when taken to the correct significant figures. This makes sense.

# Examine the Effect of Increasing and Decreasing Linear Mass Density and Tension

Do not modify the frequency from $n=4$.

1. Keeping frequency and tension constant, increase the linear mass density $\mu$ by moving the slider slowly to the right until you find another standing wave. What is the harmonic? 
    $n=5$
2. Decrease the linear mass density $\mu$ by moving the slider slowly to the left until you find another standing wave. What is the harmonic mode number of this new wave? 
    $n=3$
3. Given 1 and 2, describe how $\mu$ changes speed of the standing wave and wavelength, given a fixed frequency.
    Increasing the linear mass density will decrease the speed of the standing wave $v=\sqrt{\frac{T}{\mu}};\ v\propto\frac1\mu$. Because speed and wavelength inversely proportional when frequency is constant ($f=\frac v\lambda$) , this means that as the speed decreases, the wavelength will increase. 
4. Keeping the frequency and linear mass density constant ($\mu=2.01\times10^{-3}kg/m$), increase the tension by moving the slider slowly to the right until you find another standing wave. What is the harmonic of the new wave? 
    $n=3$
5. Decrease the tension by moving the slider slowly to the left until you find another standing wave. What is the harmonic of the new wave? 
    $n=5$
6. Given 4 and 5, explain how tension affects speed and wavelength for a fixed frequency.
    Increasing the tension will increase the speed of the wave $v=\sqrt{\frac{T}{\mu}};\ v\propto T$. Because the speed and wavelength are inversely proportional when frequency is constant ($f=\frac v\lambda$), this means that as speed increases, the wavelength will decrease.