# Rotational Kinematics, Torque, and Moment of Inertia Lab

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members:Will Caliguri, Nina Chambliss

#### Due 11/6/2020

# Predictions

1. When released the falling mass accelerates downward with a constant acceleration. While it accelerates down, will the pulley experience rotational acceleration? If so, do you think it will rotate with a constant angular acceleration? 
    The pulley should experience a uniform rotational and angular acceleration. The two values are proportional. So, if the angular acceleration is constant (which it is, due to the mass accelerating only due to gravity), then the rotational acceleration will also be constant.

2. If the amount of hanging mass $m$ is increased, will the angular acceleration of the pulley increase, decrease, or stay the same?
    The angular acceleration should increase. This is because the tangential acceleration force will be increased, due to the increased mass of the of the falling mass. 

3. Carefully sketch the angular position $\theta$ and angular velocity $\omega$ of the pulley as functions of time. Assume the pulley starts from rest at zero angular position at time 0 and accelerates uniformly. The time axes should be identical.

    ![image-20201103141135428](lab5.assets/image-20201103141135428.png)

## Rotational Kinematics

![image-20201103144329203](lab5.assets/image-20201103144329203.png)

1. Do the plots of angular position and angular speed vs time have the same shapes as you predicted in part A? How are they similar? How are they different? Explain any discrepancies.
    The graphs are different only in scaling. If the scales of the two graphs were the same, they would look functionally identical.

2. Do a linear fit to the angular speed vs time plot and record the slope, uncertainty, and units. Write in proper form.
    $$
    m=(16.27\pm0.07)rad/s^2
    $$
    
3. Apply a quadratic fit to the angular position vs time plot, and record the fitting parameters, their uncertainties, and units. Write the results in proper form.
    $$
    A=(8.11\pm0.08)rad/s^2\\
    B=(0.04\pm0.06)rad/s\\
    C=(0\pm0.9)\times10^{-3}rad
    $$
    
4. From the fitting curve parameters in 2, determine the angular acceleration and its uncertainty. 
    $$
    \alpha_{vel}=(16.27\pm0.07)rad/s^2
    $$

5. From the curve fitting parameters in 3, determine the angular acceleration and its uncertainty.
    $$
    \alpha_{pos}=(16.2\pm0.2)rad/s^2
    $$
    
6. Are the two values of the angular acceleration equal within experimental error ($\pm2(uncertainty)$)?
    Yes.
    $$
    16.3-16.2=0.1\\
    0.1<2(0.2)\\
    0.1<2(0.07)
    $$
    

# Torque and Moment of Inertia

1. Using $a_y=\alpha R$ and the value from equation 3, calculate the linear acceleration $a_y$ of the falling mass in $m/s^2$.
    $$
    a_y=\alpha_{vel}R\\
    a_y=(16.27\pm0.07)rad/s^2*(0.1m)\\
    a_y=(1.627\pm0.007)m/s^2
    $$
    
2. Using $T=m(g-\alpha R)$, calculate the magnitude of the tension in the cable in N. Is it less than the weight of the falling mass?
    $$
    T=1kg(9.81m/s^2-(1.627\pm0.007)m/s^2)\\
    T=(8.183\pm0.007)N
    $$
    
3. Using the free body diagram shown in the lab handout, write an expression for the magnitude of the net torque exerted on the pulley. The result should only depend on the tension and the radius of the pulley. 
    $$
    \tau=rF\\
    \tau=rT
    $$
    
4. From 3, calculate a numerical value for the magnitude of the net torque on the pulley in $N\times m$.
    $$
    \tau=rT\\
    \tau=0.1m*(8.183\pm0.007)N\\
    \tau=(0.8183\pm0.0007)Nm
    $$
    
5. Calculate the ratio of the net torque to the angular acceleration. 
    $$
    \frac{\tau}{\alpha}\approx\frac1{20}kgm^2\\
    \frac{\tau}{\alpha}=\frac{(0.8183\pm0.0007)Nm}{(16.27\pm0.07)rad/s^2}\\
    \frac{\tau}{\alpha}=(0.05029502151\pm0.0001726224912)kgm^2\\
    \overline{\underline{|\frac{\tau}{\alpha}=(0.0503\pm0.0002)kgm^2|}}
    $$
    
6. What physical quatity is defined by the ratio ($\tau_{net}/\alpha$)?
    $$
    N=kgms^{-2}\\
    \tau_{units}=Nm=kgm^2s^{-2};\ \alpha=rad/s^2\\
    \frac{kgm^2s^{-2}}{rad/s^{-2}}\\
    \frac{kgm{^2}\cancel{s^{-2}}}{rad/\cancel{s^{-2}}}\\
    kgm^2
    $$
    Moment of Inertia

7. Refer to the table of moments of inertia that are part of the course formula sheet. Using the mass of the pulley and its radius, calculate the moment of inertia of the pulley. Since we set the inner radius to 0mm, the disk is a uniform cylinder. Assume an uncertainty of $\pm 1mm$ for the outer radius and an uncertainty of $\pm0.1g$ for the mass of the pulley. Find the uncertainty in the moment of inertia, and write your result in proper form. How does this value compare to the value found in 6?
    $$
    I=\frac12mr^2\\
    I=\frac12(1000.0\pm0.1)g\times((0.1\pm0.001)m)^2\\
    I=
    $$
    