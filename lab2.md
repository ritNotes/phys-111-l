# Ball Drop Lab

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 9/11/2020

# Prediction

![image-20200910144244436](lab2.assets/image-20200910144244436.png)

# Analyze

## Qualitative

On each graph, locate the regions where:

- the ball is in contact with the ground.
- the ball is moving up.
- the ball is moving down.

How do these compare? Describe the similarities and differences. Explain any discrepancies.

The acceleration is different. This is because I assumed an instantaneous acceleration graph, and I made the positive acceleration longer than they were supposed to be. 

## Quantitative

### Acceleration-Time Graph

Record the Mean and standard deviation in the mean, including uncertainty. Use the correct number of significant figures and correct units. Write the result in "proper form".
$$
a=(-9.5\pm0.3)m/s^2
$$


### Velocity-Time Graph

1. Record the slope and its uncertainty exactly as it appears.
    $$
    m=-9.607\pm0.01761m/s^2
    $$

2. Write the slope and its uncertainty in "proper form". 
    $$
    m=(-9.60\pm0.02)m/s^2
    $$

3. Record the vertical intercept and its uncertainty exactly as it appears.
    $$
    b=12.78\pm0.02295m/s
    $$

4. Write the vertical intercept and its uncertainty in "proper form".
    $$
    b=(12.78\pm0.02)m/s
    $$

5. Using the fitting parameters from the velocity time graph, write the acceleration of the ball and its uncertainty while its in free fall in proper form.
    $$
    a=(-9.60\pm0.02)m/s^2
    $$
    

6. Use the "Examine" tool to determine the coordinates of two points on the velocity time graph during which the ball is in free fall. Use these coordinates to calculate the acceleration of the ball while it is in freefall. Record the data points and show the calculation. Is the value reasonable compared to what was found in #5?
    $$
    a=\frac{\Delta v}{\Delta t}\\
    a=\frac{-2.125m/s-2.200m/s}{1.55s-1.10s}\\
    a=-9.6\overline1m/s^2\\
    \overline{\underline{|a=-9.611m/s^2|}}
    $$
    This value is reasonable, compared to what has been found in previous questions.



### Position-Time Graph

1. Write the coefficients and their uncertainty exactly as they appear in the parameter box.
    $$
    A:-4.815\pm0.02636\\B:12.81\pm0.06858\\C:-7.935\pm0.04439
    $$

2. Now write the coefficients in proper form.
    $$
    A:(-4.82\pm0.03)m/s^2\\
    B:(12.81\pm0.07)m/s\\
    C:(-7.935\pm0.04)m
    $$

3. Using the curve fitting parameters from the position time graph, calculate the acceleration of the ball and its uncertainty while in free fall. Write the result in proper form.
    $$
    2A=g\\
    \therefore\\
    g=(-9.6\pm0.5)m/s^2
    $$
    

### Final Questions

1. Compare the 3 values of the acceleration that you determined in the acceleration-time graph, velocity-time graph (#5), and the position time graph (#3) . Are they equal within experimental uncertainty? If not, why not? You must justify your answer by explicitly writing the range of values for each acceleration as defined by their uncertainties.
    $$
    a_a=(-9.5\pm0.3)m/s^2\\
    a_a=(-9.2,-9.8)m/s^2\\
    a_v=(-9.60\pm0.02)m/s^2\\
    a_v=(-9.58,-9.62)m/s^2\\
    a_x=(-9.6\pm0.5)m/s^2\\
    a_x=(-9.1,-10.1)m/s^2
    $$
    All of the values are within the next largest range, so they are within error of each other. 
    

2. Are the measured accelerations equal to the gravitational acceleration $g=9.81m/s^2$ within experimental uncertainty? If not, why not?
    The measured acceleration for displacement does have $g$ fall within its error margin. However, it does not for acceleration or velocity. This could be due to inefficiencies or losses in the testing methodology, or the variance in $g$ across the earth's surface. 