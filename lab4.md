# Conservation of Angular Momentum Lab

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members:Will Caliguri, Nina Chambliss

#### Due 11/6/2020

# Prelab

1. Write down an equation that expresses the Conservation of Linear Momentum.
    $$
    p=mv
    $$
    $p_i=p_f$, given zero net external force.

2. By analogy, write down an equation for the Conservation of Angular momentum.
    $$
    p=I\omega
    $$
    $p_i=p_f$, given zero net external torque.



# Predictions

## Case 1

A student sits on the platform with their arms spread straight out and held rigid. They are pushed so that they begin to rotate. They then pull their arms in, close to their body.

### Prediction:

Will the students angular velocity change? If so, how? Why?

Their speed will increase. There is no net torque, so angular momentum is conserved. The $I$ becomes smaller ($I=\sum mr^2$), so the angular speed must increase to compensate.

### Observation:

The speed increases.

## Case 2

The student holds a small weight bar in each hand and repeats the experiment from Case 2.

### Prediction:

What will happen to the students angular velocity? Do you think the effect will be greater than, less than, or the same as in Case 1? Why?

The effect will be the same, because the mass does not change for the duration of the movement, only the radius does. 

### Observation:

The speed increases by a greater amount. 

## Case 3

A student stands on a platform that is initially at rest. A bicycle wheel which is rapidly rotating in the vertical plaine is handed to the student. The student on the platform flips the axis of the wheel by $90^\circ$, so the wheel is now rotating in the horizontal plane.

### Prediction:

Will the platform with the student on it begin to rotate? If so, in what direction? Why?

The platform will rotate in the opposite direction the wheel does, and at a slower speed. This is to counteract the rotation of the rotational axis, making the initial and final momentums the same. 

### Observation:



## Case 4

The student on the platform described in Case 3 flips the axis of the wheel by $180^\circ$, so that the wheel is again rotating in the horizontal plane, but is now rotating in the opposite direction. 

### Prediction:

Will the platform's angular velocity change? Why?

The angular velocity will become inverted. You still have to counteract the rotation of the rotational axis, but this time it has to be in the opposite direction to Case 3.

### Observation:

