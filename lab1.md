# Basic Measurements and Uncertainties Lab

## Skyler MacDougall

## PHYS-111 2pm Lab

### Group Members: Will Caliguri, Nina Chambliss, Caroline Cody

#### Due: 8/24/2020

# Method

You will learn a basic technique to determine uncertainties in measured quantities and how to use those uncertainties in calculated quantities. Specifically, you will measure the spacial dimensions of a typical soda-can, approximating a cylinder. You will use this data to calculate the volume and density of the cylinder. A volume correction factor will be supplied by your instructor for the uneven shape at the top and bottom of the soda-cans. The uncertainties in your measurements will be used to determine the uncertainties in the calculated volume and density.
$$
Volume\ Correction\ Factor:-42.00cm^3
$$
Record the product name of your soda-can and the quantity in oz and/or ml that is shown on the label.

Name and quantity: Dr Pepper, 12 oz (335mL)

# Measurements

What is the least count of your ruler? 1mm

You will use the ruler to measure the widest diameter and longest height of the soda can. Note that soda cans have narrower regions at the top and bottom of the can. Ensure to measure properly. Height should be measured from the beginning of the vertical wall to the rim of the top. Diameter should be measured across the top of the can.

Measure each dimension 5 times across different points on the can. (This can be done by rotating the can to measure across different points). Then, calculate the mean (average) value for each dimension, and the average deviation from the mean. use either the average deviation from the mean or $\frac12$ the least count, whichever is larger, as your uncertainty.

| Measurement # | Diameter ($D$, mm) | Height ($H$, mm) |
| ------------- | ------------------ | ---------------- |
| 1             | 59.5               | 114.0            |
| 2             | 57.0               | 114.7            |
| 3             | 58.0               | 115.0            |
| 4             | 59.0               | 115.1            |
| 5             | 58.5               | 114.5            |

| Label              | Mean Value | Average Deviation | $\frac12$ least count | Uncertainty |
| ------------------ | ---------- | ----------------- | --------------------- | ----------- |
| Diameter ($D$, mm) | 58.4       | 0.72              | 0.5                   | 0.72        |
| Height ($H$, mm)   | 114.86     | 0.208             | 0.5                   | 0.5         |

# Calculations

$$
Volume\ of\ a\ Cylinder:\\
V=\pi(\frac D2)^2H
$$

1. 
    1. Use the mean (average) values of diameter and the height to calculate the mean volume:
        $$
        V=\pi(\frac{D}{2})^2H\\
        V=\pi(\frac{58.4mm}{2})^2(117.86mm)\\
        V=\pi((\frac{58.4}{2})^2mm^2)(114.86mm)\\
        V=(\pi(\frac{58.4}{2})^2(114.86))mm^3\\
        (excel\ spreadsheet\ used\ for\ calculations)\\
        V=307669.45875961mm^3\\
        \overline{|V=3.08\times10^5mm^3|}\\
        \underline{{|V=3.08\times10^2cm^3|}}
        $$
    
    2. Calculate the uncertainty in volume ($\delta V=V_{max}-V_{mean}$):
        $$
        V=\pi(\frac{D}{2})^2H\\
        V_{max}=\pi(\frac{D+\delta D}{2})^2((H+\delta H))\\
        V_{max}=\pi(\frac{58.4mm+0.72mm} {2} )^2 (( 114.86mm +0.5mm ))\\
        V_{max}=\pi((\frac{58.4+0.72}{2})^2mm^2)((114.86+0.5)mm)\\
        V_{max}=(\pi(\frac{59.12}{2})^2(115.36))mm^3\\
        (excel\ spreadsheet\ used\ for\ calculations)\\
        V_{max}=316675.146048709mm^3\\
        \delta V=V_{max}-V_{mean}\\
        \delta V=316675.146048709mm^3-307669.45875961mm^3\\
        \delta V=9005.68728909933mm^3\\
        \overline{|\delta V=0.09\times10^5mm^3|}\\
        \underline{|\delta V=0.09\times10^2cm^3|}
        $$
    
    3. Write the volume in "Proper form" $(V_{mean}\pm\delta V)\ units$:
        $(3.08\pm0.09)\times10^2cm^3$

2. **Corrected volume**
    Recall the uneven shape at the top and bottom of your soda can. THis affects the volume. To obtain a more accurate value for the volume, we need to correct for the effect of that unevenness.
    For a standard soda can, the correction factor is $-42.00cm^3$. Subract this factor from the best estimate of your volume. Leave the uncertainty as it is.

    1. Write the corrected volume in "proper form" $(V_{corrected}\pm\delta V)units$:
        $(2.66\pm0.09)\times10^2cm^3$
    2. The corrected volume of your can is now known experimentally to within $\pm2$ deviations. Write the range of possible values of the corrected range.
        $Range=(247,285)cm$

3. **Comparison to Electronic Caliper**
    There are instruments that can provide much more precise measurements than mm rulers. An electronic caliper is such an instrument, and it was used to provide the following measurements on a soda can. 

    ![image-20200903130030041](lab1.assets/image-20200903130030041.png)

    1. Does your corrected volume agree with the volume obtained by the caliper measurements, within experimental uncertainty?
        No, it is not.
    2. Which result of volume would you expect to be more accurate, the result based on the ruler, or the result based on the caliper? Why?
        The caliper would be more accurate, since the calipers can actually contact the surface of the can, while the ruler relies on the human eye, which can fail or read inaccurately.

4. **Density comparison**
    Mass density $\rho$ is defined as the mass of the specimen divided by its volume:
    $$
    \rho=\frac mV
    $$
    This property is intrinsic to any material. The material inside a soda can is mostly water, with sugar and a few other ingredients often added. The can itself is a thin sheet of aluminum.
    Density values of materials are tabulated in many handbooks; for example, the density of water at room temperature is $0.997g/cm^3$. The mass of an unopened standard soda can was measured using a mass scale. The value of the mass was found to be $m=(375\pm1)g$.

    1. Using this mass and the corrected volume of your can, calculate the mean density $\rho$ of your can with its contents, in $g/cm^3$.
        $$
        \rho=\frac{m}{V}\\
        \rho=\frac{(375\pm1)g}{(2.66\pm0.09)\times10^2cm^3}\\
        \rho=\frac{(375\pm1)}{(2.66\pm0.09)\times10^2}g/cm^3\\
        \rho_{mean}=\frac{375}{2.66\times10^2}g/cm^3\\
        \rho_{mean}=0.708g/cm^3\\
\rho_{max}=\frac{376}{2.75\times10^2}g/cm^3\\
        \rho_{max}=0.731g/cm^3\\
        \delta\rho=\rho_{max}-\rho_{mean}\\
        \delta\rho=0.731g/cm^3-0.708g/cm^3\\
        \delta\rho=0.029g/cm^3\\
        \rho=(0.71\pm0.03)g/cm^3
    $$
        
    2. Calculate the percent difference between your mean density and the handbook value for water.
        $$
        difference\ in\ density=\rho_{mean}-\rho_{handbook}\\
        difference\ in\ density=0.71g/cm^3-0.997g/cm^3\\
        difference\ in\ density=0.29g/cm^3\\
    \%\ difference=29\%
        $$
        
    3. Roughly compare your mean density in 4a to that obtained by other students in your work group. If you and the other students seem to have density values that are closer to each other than to the textbook value, what do you think contributes to the largest error in this density calculation, your measurements of volume or the mass given above? Briefly explain your reasoning.
        The other students in my work group had densities higher than mine. 
